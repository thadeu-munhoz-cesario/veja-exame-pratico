window.onload = function () {

    //Consumindo API Posts
    let ENDPOINT_POSTS = 'https://public-api.wordpress.com/rest/v1.1/sites/109720969/posts?number=10';
    // let ENDPOINT_POSTS = 'https://public-api.wordpress.com/rest/v1.1/sites/109720969/posts?number=10&page=1';
    let RESPONSE_ENDPOINT_POSTS = new XMLHttpRequest();

    RESPONSE_ENDPOINT_POSTS.open('GET', ENDPOINT_POSTS);
    RESPONSE_ENDPOINT_POSTS.responseType = 'json';
    RESPONSE_ENDPOINT_POSTS.send();

    RESPONSE_ENDPOINT_POSTS.onload = function () {
        let resp = RESPONSE_ENDPOINT_POSTS.response;
        let postLength = 0;
        laodPosts(resp, postLength);
    }


    //Menu Mobile
    var hamburger = document.querySelector(".hamburger");
    var header = document.querySelector(".header-nav");

    hamburger.addEventListener("click", function() {
        hamburger.classList.toggle("is-active");
        header.classList.toggle("show");
    });
}

async function laodPosts(resp, postLength){
    let posts = resp.posts;
    let $postsNode = $('#js-main-post');
    
    for(let iterator = postLength; iterator < posts.length; iterator++){
        let currentPostImage = posts[iterator]['featured_image'];
        let currentPostAddress = posts[iterator]['URL'];
        let currentPostData = posts[iterator]['date'];
        let currentPostTitle = posts[iterator]['title'];
        let currentPostAuthor = posts[iterator]['author']['name'];
        let currentPostAuthorImage = posts[iterator]['author']['avatar_URL'];
        let currentPostAuthorPage = posts[iterator]['author']['profile_URL'];

        let currentTypeCategory = Object.keys(posts[iterator]['categories'])[0];
        let currentPostCategories = posts[iterator]['categories'][currentTypeCategory]['name'];

        if(currentPostCategories){
            await $postsNode.append(
                `
                <div class="content-posts__general" style="display:none">
                    <div class="content-posts__img">
                        <a class="content-posts__link" href="${currentPostAddress}">
                            <img class="content-posts__photo lazyload" data-src="${currentPostImage}" alt="${currentPostTitle}">
                        </a>
                    </div>
                    <div class="content-posts__details">
                        <p class="content-posts__category">${currentPostCategories}</p>
                        <a href="${currentPostAddress}" class="content-posts__title">${currentPostTitle}</a>
                        <p class="content-posts__description">${currentPostData}</p>
                        <div class="content-posts__author">
                            <a href="${currentPostAuthorPage}" class="content-posts__author-link">
                                <img data-src="${currentPostAuthorImage}" alt="${currentPostAuthorImage}" class="content-posts__author-image lazyload"/>
                                <p class="content-posts__author-name">${currentPostAuthor}</p>
                            </a>
                        </div>
                    </div>
                </div>
                <hr class="content-posts__line">
                `
            );
        }
        else{
            await $postsNode.append(
                `
                <div class="content-posts__general" style="display:none">
                    <div class="content-posts__img">
                        <a class="content-posts__link" href="${currentPostAddress}">
                            <img class="content-posts__photo lazyload" data-src="${currentPostImage}" alt="${currentPostTitle}">
                        </a>
                    </div>
                    <div class="content-posts__details">
                        <p class="content-posts__category"></p>
                        <a href="currentPostAddress" class="content-posts__title">${currentPostTitle}</a>
                        <p class="content-posts__description">${currentPostData}</p>
                        <div class="content-posts__author">
                            <a href="${currentPostAuthorPage}" class="content-posts__author-link">
                                <img data-src="${currentPostAuthorImage}" alt="${currentPostAuthorImage}" class="content-posts__author-image lazyload"/>
                                <p class="content-posts__author-name">${currentPostAuthor}</p>
                            </a>
                        </div>
                    </div>
                </div>
                <hr class="content-posts__line">
                `
            );
        }
    }
    
    lazyLoad(); 
    if(postLength <= 10){
        await loadFuncionality();
    }
    updateTime();
    
}

async function loadFuncionality(){
    let $footer = $('#js--footer');
    let $btnLoad = $('#js--load-more');
    let $advPopUp = $('#promotion-popup');
    await $footer.show();
    await $btnLoad.show();
    await $advPopUp.show();
    await eventLoadPhoto();
    return true;
}

async function eventLoadPhoto(){
    let btnLoadPictures = document.getElementById('js--load-more');
    
    btnLoadPictures.onclick = await function(){
        let lengthPosts = document.getElementsByClassName('content-posts__general').length;
        let totalPosts = lengthPosts + 10;
        
        let ENDPOINT_POSTS_NEW = `https://public-api.wordpress.com/rest/v1.1/sites/109720969/posts?number=${totalPosts}`;
        let RESPONSE_ENDPOINT_POSTS_NEW = new XMLHttpRequest();

        RESPONSE_ENDPOINT_POSTS_NEW.open('GET', ENDPOINT_POSTS_NEW);
        RESPONSE_ENDPOINT_POSTS_NEW.responseType = 'json';
        RESPONSE_ENDPOINT_POSTS_NEW.send();

        RESPONSE_ENDPOINT_POSTS_NEW.onload = function () {
            let resp = RESPONSE_ENDPOINT_POSTS_NEW.response;
            laodPosts(resp, lengthPosts);
        }
    }
}

function updateTime(){
    let dataTime = document.getElementsByClassName('content-posts__description');
    for(var iterator = 0; iterator < dataTime.length; iterator++){
        try{
            let dataCurrent = (dataTime[iterator].textContent).split('T');
            let dateYMD = dataCurrent[0].split('-');
            let dateTime = dataCurrent[1].split(':');
            let replaceMonth = null;
    
            switch(dateYMD[1]){
                case '01':
                    replaceMonth = 'Jan';
                    break;
        
                case '02':
                    replaceMonth = 'Fev';
                    break;
        
                case '03':
                    replaceMonth = 'Mar';
                    break;
        
                case '04':
                    replaceMonth = 'Abr';
                    break;
        
                case '05':
                    replaceMonth = 'Mai';
                    break;
        
                case '06':
                    replaceMonth = 'Jun';
                    break;
        
                case '07':
                    replaceMonth = 'Jul';
                    break;
        
                case '08':
                    replaceMonth = 'Ago';
                    break;
        
                case '09':
                    replaceMonth = 'Set';
                    break;
        
                case '10':
                    replaceMonth = 'Out';
                    break;
        
                case '11':
                    replaceMonth = 'Nov';
                    break;
        
                case '12':
                    replaceMonth = 'Dez';
                    break;
            }
        
            let finalDate = `${dateYMD[dateYMD.length-1]} ${replaceMonth} ${dateYMD[0]}`;
            let finalTime = `${dateTime[0]}h${dateTime[1]}`
            dataTime[iterator].innerHTML = `
            <div class="icon-time">
                <svg class="icon-time__svg" enable-background="new 0 0 443.294 443.294" height="20" viewBox="0 0 443.294 443.294" width="512" xmlns="http://www.w3.org/2000/svg"><path d="m221.647 0c-122.214 0-221.647 99.433-221.647 221.647s99.433 221.647 221.647 221.647 221.647-99.433 221.647-221.647-99.433-221.647-221.647-221.647zm0 415.588c-106.941 0-193.941-87-193.941-193.941s87-193.941 193.941-193.941 193.941 87 193.941 193.941-87 193.941-193.941 193.941z"/><path d="m235.5 83.118h-27.706v144.265l87.176 87.176 19.589-19.589-79.059-79.059z"/></svg>
                <span class='icon-time__data'>${finalDate},</span> <span class='icon-time__time'>${finalTime}</span>
            </div>
            `;
        }catch(e){

        }
    }
    return true;
}



function lazyLoad(){
	window.addEventListener("scroll", function() { onScrollDiv() });
	window.addEventListener("DOMContentLoaded", function() { onScrollDiv() });
    onScrollDiv();
	function onScrollDiv() {
		var images = document.querySelectorAll('.lazyload');
		for (var i=0, nb=images.length ; i <nb ; i++) {
			var img = images[i]
			var rect = img.getBoundingClientRect();
			var isVisible = ((rect.top - window.innerHeight) < 500 && (rect.bottom) > -50 ) ? true : false ;

			if (isVisible) {
				if (!img.src) {
					img.src = img.dataset.src;
				}
			}
        }
        $('.content-posts__general').show();
    }
    window.removeEventListener("scroll", function() { onScrollDiv() });
	window.removeEventListener("DOMContentLoaded", function() { onScrollDiv() });

}



